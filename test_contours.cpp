#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/core.hpp>
#include <iostream>

#include "BlobGetter.h"

using namespace std;

static int selected_x, selected_y;
static bool clicked = false;

void onMouse(int event,int x,int y, int flags,void* param) 
{	
    if (event != cv::EVENT_LBUTTONDOWN) return;

	if (selected_x != x || selected_y != y)
	{
		selected_x = x;
		selected_y = y;
        // cout << x << " - " << y << endl;
        clicked = true;
	}
}


int main(int argc, char** argv)
{
    string path = "images/street_001.jpg";
    cv::Mat img = cv::imread(path);

    BlobGetter blobGetter = BlobGetter();
    // blobGetter = std::dynamic_pointer_cast<Camera>();
    cv::Mat blobs;
    cv::Rect bbox;

    while(true)
    {
        cv::namedWindow("Image", cv::WINDOW_NORMAL);
        cv::setMouseCallback("Image", onMouse, &img);
        cv::Mat selection;
        cv::Rect selectRect;

        if (clicked == true)
        {
            cout << "x=" << selected_x << " - " << "y=" << selected_y << endl;
            clicked = false;

            int x0 = max(selected_x - 5,0);
            int y0 = max(selected_y-5,0);
            int w = min(img.cols-x0, 10);
            int h = min(img.rows-y0, 10);

            selectRect = cv::Rect(x0, y0, w, h);
            // cout << "selected: " << selectRect << endl;
            selection = img(selectRect);

            bbox = blobGetter.GetBlobs(img, selected_x, selected_y, selection);
            // imshow("Selection", blobs);
        }

        cv::rectangle(img, bbox.tl(), bbox.br(), cv::Scalar(0,0,255), 4);
        cv::imshow("Image", img);
        int userkey = cv::waitKey(50);
        if (userkey == 'q' || userkey == 27) break;
    }

    return 0;
}

