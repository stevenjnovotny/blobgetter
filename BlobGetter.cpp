#include "BlobGetter.h"


BlobGetter::BlobGetter()
{
    cout << "initializing BlobGetter" << endl;
}

cv::Rect BlobGetter::GetBlobs(cv::Mat& img, int x, int y, cv::Mat& selection)
{
    // cout << "Image of size:" << img.rows << " x " << img.cols << endl;
    
    // convert selection to colorspace
    cv::Mat cvt;
    // cvtColor(selection, cvt, COLOR_BGR2HSV);
    cvt = selection;   
    cv::Mat imgBlur;
    GaussianBlur(cvt, imgBlur, cv::Size(5,5), 3, 0); 
    vector<cv::Mat> color_split;
    cv::split(imgBlur, color_split);
    cv::Mat c1_sel = color_split[0];
    cv::Mat c2_sel = color_split[1];
    cv::Mat c3_sel = color_split[2];

    // calculate mean and standard deviation over selection in chosen color space
    cv::Scalar c1_mean, c1_stddev, c2_mean, c2_stddev, c3_mean, c3_stddev;
    cv::meanStdDev(c1_sel, c1_mean, c1_stddev);
    cv::meanStdDev(c2_sel, c2_mean, c2_stddev);
    cv::meanStdDev(c3_sel, c3_mean, c3_stddev);

    // cout << "c1 Mean: " << c1_mean[0] << "   StdDev: " << c1_stddev[0] << endl;
    // cout << "c2 Mean: " << c2_mean[0] << "   StdDev: " << c2_stddev[0] << endl;
    // cout << "c3 Mean: " << c3_mean[0] << "   StdDev: " << c3_stddev[0] << endl;
    
    // convert image to colorspace  (or leave as BGR)
    // cvtColor(img, cvt, COLOR_BGR2HSV);
    cvt = img;

    cv::Mat blobs(cvt.cols, cvt.rows, CV_8UC3, cv::Scalar(0));
    cv::Scalar lower(static_cast<int>(c1_mean[0] - c1_stddev[0]), static_cast<int>(c2_mean[0] - c2_stddev[0]), static_cast<int>(c3_mean[0] - c3_stddev[0]));
    cv::Scalar upper(static_cast<int>(c1_mean[0] + c1_stddev[0]), static_cast<int>(c2_mean[0] + c2_stddev[0]), static_cast<int>(c3_mean[0] + c3_stddev[0]));

    cv::inRange(cvt, lower, upper, blobs);
    cv::Mat imgCanny;
    cv::Canny(blobs, imgCanny, 75, 200);
    cv::Mat blobsDil;
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(7,7));
    cv::dilate(imgCanny, blobsDil, kernel);  // fills gaps


    cv::Rect bbox = getContours(blobsDil, x, y);

    // rectangle(img, bbox.tl(), bbox.br(), Scalar(0,0,255), 4);

    return bbox;
}

cv::Rect BlobGetter::getContours(cv::Mat img, int x, int y) 
{
    vector<vector<cv::Point>> contours;
    vector<cv::Vec4i> hierarchy;

    cv::Mat blobs = cv::Mat::zeros( img.size(), CV_8UC1);
    cv::Mat mask = cv::Mat::zeros( img.size(), CV_8UC1);        

    int subregion_w = (int)(img.cols/4.0f);
    int subregion_h = (int)(img.rows/4.0f);

    int x0 = max(x-subregion_w/2, 0);
    int y0 = max(y-subregion_h/2, 0);
    int w = min(subregion_w, img.cols - x0 - 1);
    int h = min(subregion_h, img.rows - y0 - 1);


    //cv::Rect subregion(x-subregion_w/2, y-subregion_h/2, subregion_w, subregion_h);
    cv::Rect subregion(x0, y0, w, h);
    // cout << "subregion: " << subregion << endl;
    rectangle(mask, subregion.tl(), subregion.br(), cv::Scalar(255), -1);

    cv::bitwise_and(img, mask, blobs);

    cv::findContours(blobs, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    // cout << "number of contours: " << contours.size() << endl;

    cv::Rect boundRect;   
    vector<cv::Point> maxContour;
    bool objFound = false;
    int maxArea = (int)(img.rows/4) * (int)(img.cols/4);

    cv::Rect bbox;
    for(int i = 0; i < contours.size(); i++) {
        int area = cv::contourArea(contours[i]);
        bbox = cv::boundingRect(contours[i]);
        if(bbox.x < x && (bbox.x + bbox.width) > x && bbox.y < y && (bbox.y + bbox.height) > y)
        {
            if (area > 10 && area < maxArea)
            {
                maxArea = area;
                maxContour = contours[i];
                objFound = true;
            }
        }
    }
    if (objFound) {
        boundRect = cv::boundingRect(maxContour);
    }
    return boundRect;
}
