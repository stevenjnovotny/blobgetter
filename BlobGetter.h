#ifndef BLOB_GETTER_H
#define BLOB_GETTER_H

#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/core.hpp>
#include <iostream>

#include <vector>  
#include <tuple> 


using namespace std;


class BlobGetter
{
    public:
        BlobGetter();
        cv::Rect GetBlobs(cv::Mat& img, int x, int y, cv::Mat& selection);

    private: 
        cv::Rect getContours(cv::Mat img, int x, int y);
};


#endif