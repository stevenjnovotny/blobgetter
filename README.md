# Use Thresholding and contours to select ROI #

This code takes in an image, an (x,y) point, and a region centered on the x-y point to select an object. Overall the flow is as follows:

- User clicks on an object in an image
- a 10x10 pixel selection region is established around that click
- The img, the point, and the selected region are passed to the BlobGetter
- The image is split into channels and color statistics are established for each
- the image is thresholded using the statistics
- edges are found and dilated
- conturs are taken in an area that is no bigger than img_width/4 and img_height/4
- the largest contour containing the original mouse click is found
- bounding box is created and returned

An example is shown below:

![Ex Image](select_example.png)